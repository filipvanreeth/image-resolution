<?php

use Filipvanreeth\ImageResolution\ImageResolution;

/**
 * Plugin Name: Image Resolution
 * Description: Shows the image resolution of the WordPress full image in PPI in the media library list view. A helpful tool for identifying and cleanup (faulty) high resolution images in the media library. The plugin makes use of the Imagick PHP extension.
 * Version: 1.0.0
 * Plugin URI: https://gitlab.com/filipvanreeth/image-resolution/
 * Author: Filip Van Reeth
 * Author URI: https://filipvanreeth.com/
 * License: GPLv2 or later
 * Text Domain: image-resolution
 * Requires PHP: 7.4
 */

// Exit if accessed directly
if (! defined('ABSPATH')) {
    exit;
}

// Constants
define('FILIPVANREETH_IMAGE_RESOLUTION_DIR', plugin_dir_path(__FILE__));
define('FILIPVANREETH_IMAGE_RESOLUTION_URL', plugin_dir_url(__FILE__));

// Composer autoload
if (file_exists(plugin_dir_path(__FILE__) . 'vendor/autoload.php')) {
    require plugin_dir_path(__FILE__) . 'vendor/autoload.php';
}

/**
 * Loads Image Resolution instance
 * @return void
 */
function filipvanreethImageResolution()
{
    ImageResolution::instance()->init();
}

filipvanreethImageResolution();
