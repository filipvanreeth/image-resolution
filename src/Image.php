<?php

namespace Filipvanreeth\ImageResolution;

use Imagick;

/**
 * Image
 * @package Filipvanreeth\ImageResolution
 */
class Image
{
    private string $file;
    private ?int $resolution = null;

    public function __construct($file)
    {
        $this->file = $file;
        $this->setResolution();
    }

    public function setFile(string $file): void
    {
        $this->file = $file;
    }

    public function getResolution(): ?int
    {
        return $this->resolution;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    private function setResolution(): void
    {
        $file = $this->file;
        $imagick = new Imagick($file);

        $resolution = $imagick->getImageResolution();
        $units = $imagick->getImageUnits();

        if ($units == Imagick::RESOLUTION_PIXELSPERCENTIMETER) {
            $resolution['x'] = (int)round($resolution['x'] * 2.54);
            $resolution['y'] = (int)round($resolution['y'] * 2.54);
            $imagick->setImageUnits(Imagick::RESOLUTION_PIXELSPERINCH);
            $imagick->setImageResolution($resolution['x'], $resolution['y']);
            $resolution = $imagick->getImageResolution();
        }

        $this->resolution = (int) $resolution['x'];
    }
}
