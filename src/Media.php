<?php

namespace Filipvanreeth\ImageResolution;

use Filipvanreeth\ImageResolution\Image;

class Media
{
    public function init()
    {
        add_filter('manage_media_columns', [$this, 'manageMediaColumns'], 1);
        add_action('manage_media_custom_column', [$this, 'manageMediaCustomColumn'], 1, 2);
        add_action('admin_enqueue_scripts', [$this, 'adminEnqueueScripts']);
    }

    public function adminEnqueueScripts()
    {
        if ('upload' == get_current_screen()->base) {
            $file = 'assets/dist/css/style.css';
            wp_register_style('image-resolution', FILIPVANREETH_IMAGE_RESOLUTION_URL . $file, [], filemtime(FILIPVANREETH_IMAGE_RESOLUTION_DIR . $file));
            wp_enqueue_style('image-resolution');
        }
    }

    public function manageMediaColumns($columns)
    {
        $columns['resolution'] = __('Resolution', 'image-resolution');
        return $columns;
    }

    public function manageMediaCustomColumn($column, $id)
    {
        if ($column === 'resolution') {
            $file = get_attached_file($id);
            $image = new Image($file);
            $resolution = $image->getResolution();

            if (0 == $resolution) {
                echo '<span class="image-resolution image-resolution--warning">' . __('undefined', 'image-resolution') . '</span>';
                return;
            }

            if (72 < $resolution) {
                $class = 'image-resolution--error';
            } else {
                $class = 'image-resolution--fine';
            }

            echo '<span class="image-resolution ' . esc_attr($class) . '">' . esc_attr($resolution) . ' ppi</span>';
        }
    }
}
