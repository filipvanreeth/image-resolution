<?php

namespace Filipvanreeth\ImageResolution;

/**
 * Image Resolution
 * @package Filipvanreeth\ImageResolution
 */
class ImageResolution
{
    public function init(): void
    {
        add_action('init', array( $this, 'actionInit' ));
        $this->loadMedia();
    }

    public function actionInit(): void
    {
        # Actions
        load_plugin_textdomain('image-resolution', false, plugin_basename(dirname(__DIR__)) . '/languages');

        # Filters
    }

    /**
     * Loads
     * @return void
     */
    private function loadMedia()
    {
        if (false == self::isImagickLoaded()) {
            add_action('admin_notices', [$this, 'imagickNotLoadedAdminNoticeError']);
            return;
        }

        $media = new Media();
        $media->init();
    }

    public static function isImagickLoaded()
    {
        if (!extension_loaded('imagick')) {
            return false;
        }

        return true;
    }

    public function imagickNotLoadedAdminNoticeError()
    {
        $class = 'notice notice-error';
        $message = __('The Imagick PHP extension could not be loaded.', 'image-resolution');

        printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    }

    public static function instance()
    {
        static $instance = null;

        if (! $instance) {
            $instance = new ImageResolution();
        }

        return $instance;
    }
}
