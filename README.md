# Image Resolution WordPress plugin

Shows the image resolution of the WordPress full image in PPI in the media library list view. A helpful tool for identifying and cleanup (faulty) high resolution images in the media library. The plugin makes use of the Imagick PHP extension.

## Installation

The recommended installation way is through Composer.

```
$ composer require filipvanreeth/image-resolution
```

You can also install or upload the plugin manually into the WordPress plugin directory. Just run the following command into the ìmage-resolution` plugin folder to install the Composer dependencies:

```
$ composer install --no-dev -O
```
